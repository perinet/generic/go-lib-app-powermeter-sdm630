module gitlab.com/perinet/generic/apiservice/powermetersdm630

go 1.18

require (
	actshad.dev/modbus v0.2.1
	gitlab.com/perinet/generic/apiservice/dnssd v0.0.0-20230303101032-12faee2d9b44
	gitlab.com/perinet/generic/lib/httpserver v0.0.0-20221122163058-59cb209fd3f2
	gitlab.com/perinet/generic/lib/utils v0.0.0-20221122162821-91b714770155
	gitlab.com/perinet/periMICA-container/apiservice/node v0.0.0-20221130091942-a456b86bb5af
)

require (
	github.com/alexandrevicenzi/go-sse v1.6.0 // indirect
	github.com/felixge/httpsnoop v1.0.3 // indirect
	github.com/goburrow/serial v0.1.0 // indirect
	github.com/godbus/dbus/v5 v5.1.0 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/handlers v1.5.1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/holoplot/go-avahi v1.0.1 // indirect
	gitlab.com/perinet/periMICA-container/apiservice/lifecycle v0.0.0-20221101161231-d517bb9ab0d3 // indirect
	golang.org/x/exp v0.0.0-20221012211006-4de253d81b95 // indirect
)
