/*
 * Copyright (c) 2018-2022 Perinet GmbH
 * All rights reserved
 *
 * This software is dual-licensed: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation. For the terms of this
 * license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
 * or contact us at https://server.io/contact/ when you want license
 * this software under a commercial license.
 */

package powermetersdm630

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"sync"

	server "gitlab.com/perinet/generic/lib/httpserver"
	rbac "gitlab.com/perinet/generic/lib/httpserver/rbac"
	filestorage "gitlab.com/perinet/generic/lib/utils/filestorage"
	webhelper "gitlab.com/perinet/generic/lib/utils/webhelper"
	node "gitlab.com/perinet/periMICA-container/apiservice/node"
)

type PathInfo = server.PathInfo

type Sdm630Register struct {
	Address uint16
	Unit    string
}

type Measure struct {
	Value float32 `json:"value"`
	Unit  string  `json:"unit"`
}

const (
	API_VERSION = "20"
)

// Data type for PowermeterSDM630Info_Get
type PowermeterSDM630Info struct {
	ApiVersion string `json:"api_version"`
}

// Data type for PowermeterSDM630Config_Get/Set
type PowermeterSDM630Config struct {
	Hostname   string  `json:"hostname"`
	RtuAddress int32   `json:"rtu_address"`
	SampleRate float32 `json:"sample_rate"`
}

// Data type for PowermeterSDM630Sample_Get
type PowermeterSDM630Sample struct {
	L1Voltage    Measure `json:"l1_voltage"`
	L2Voltage    Measure `json:"l2_voltage"`
	L3Voltage    Measure `json:"l3_voltage"`
	L1Current    Measure `json:"l1_current"`
	L2Current    Measure `json:"l2_current"`
	L3Current    Measure `json:"l3_current"`
	L1Power      Measure `json:"l1_power"`
	L2Power      Measure `json:"l2_power"`
	L3Power      Measure `json:"l3_power"`
	L1PhaseAngle Measure `json:"l1_phase_angle"`
	L2PhaseAngle Measure `json:"l2_phase_angle"`
	L3PhaseAngle Measure `json:"l3_phase_angle"`
	TotalImport  Measure `json:"total_import"`
	TotalExport  Measure `json:"total_export"`
}

type sampleCache struct {
	sample PowermeterSDM630Sample
	mutex  sync.Mutex
}

var (
	// SDM630 input registers
	SdmRegisters = map[string]Sdm630Register{
		"L1Voltage":    {0x00, "V"},
		"L2Voltage":    {0x02, "V"},
		"L3Voltage":    {0x04, "V"},
		"L1Current":    {0x06, "A"},
		"L2Current":    {0x08, "A"},
		"L3Current":    {0x0A, "A"},
		"L1PowerW":     {0x0C, "W"},
		"L2PowerW":     {0x0E, "W"},
		"L3PowerW":     {0x10, "W"},
		"L1PhaseAngle": {0x24, "°"},
		"L2PhaseAngle": {0x26, "°"},
		"L3PhaseAngle": {0x28, "°"},
		"TotalImport":  {0x48, "kWh"},
		"TotalExport":  {0x4A, "kWh"},
	}

	mbus_config = PowermeterSDM630Config{Hostname: "", RtuAddress: 1, SampleRate: 1} // Default config
	nodeInfo    node.NodeInfo

	Logger log.Logger = *log.Default()
)

const (
	MBUS_CONFIG_FILE = "/var/lib/powermeter-sdm630/mbus.json"
)

func init() {
	Logger.SetPrefix("ApiServicePowerMeterSDM630: ")
	Logger.Println("Starting")

	filestorage.LoadObject(MBUS_CONFIG_FILE, &mbus_config)

	data := webhelper.InternalGet(node.NodeInfoGet)

	err := json.Unmarshal(data, &nodeInfo)
	if err != nil {
		Logger.Println(err)
		return
	}
	go keep_mbus_connection()
	go collect_mbus_samples()
}

// Deliver API endpoints of powermeter-sdm630
func PathsGet() []PathInfo {
	return []PathInfo{
		{Url: "/powermeter-sdm630", Method: server.GET, Role: rbac.NONE, Call: PowermeterSDM630Info_Get},
		{Url: "/powermeter-sdm630/config", Method: server.GET, Role: rbac.ADMIN, Call: PowermeterSDM630Config_Get},
		{Url: "/powermeter-sdm630/config", Method: server.PATCH, Role: rbac.SUPERUSER, Call: PowermeterSDM630Config_Set},
		{Url: "/powermeter-sdm630/sample", Method: server.GET, Role: rbac.USER, Call: PowermeterSDM630Sample_Get},
	}
}

// GET handler for /powermeter-sdm630 endpoint
func PowermeterSDM630Info_Get(w http.ResponseWriter, r *http.Request) {
	var http_status int = http.StatusOK
	var info = PowermeterSDM630Info{
		ApiVersion: API_VERSION,
	}
	res, err := json.Marshal(info)
	if err != nil {
		http_status = http.StatusInternalServerError
		res = json.RawMessage(`{"error": "` + err.Error() + `"}`)
	}
	webhelper.JsonResponse(w, http_status, res)
}

// GET handler for /powermeter-sdm630/config endpoint
func PowermeterSDM630Config_Get(w http.ResponseWriter, r *http.Request) {
	var http_status int = http.StatusOK
	data, err := json.Marshal(mbus_config)
	if err != nil {
		http_status = http.StatusInternalServerError
		data = json.RawMessage(`{"error": "` + err.Error() + `"}`)
	}
	webhelper.JsonResponse(w, http_status, data)
}

// PATCH handler for /powermeter-sdm630/config endpoint
func PowermeterSDM630Config_Set(w http.ResponseWriter, r *http.Request) {
	payload, _ := io.ReadAll(r.Body)
	var mbus_cfg PowermeterSDM630Config
	err := json.Unmarshal(payload, &mbus_cfg)
	if err != nil {
		webhelper.EmptyResponse(w, http.StatusBadRequest)
		return
	}

	err = filestorage.StoreObject(MBUS_CONFIG_FILE, mbus_cfg)
	if err != nil {
		webhelper.EmptyResponse(w, http.StatusBadRequest)
		return
	}

	webhelper.EmptyResponse(w, http.StatusNoContent)
}

// GET handler for /powermeter-sdm630/sample endpoint
func PowermeterSDM630Sample_Get(w http.ResponseWriter, r *http.Request) {
	var http_status int = http.StatusOK

	sample_cache.mutex.Lock()
	sample := sample_cache.sample
	sample_cache.mutex.Unlock()

	data, err := json.Marshal(sample)
	if err != nil {
		http_status = http.StatusInternalServerError
		data = json.RawMessage(`{"error": "` + err.Error() + `"}`)
	}
	webhelper.JsonResponse(w, http_status, data)
}
