/*
 * Copyright (c) 2018-2022 Perinet GmbH
 * All rights reserved
 *
 * This software is dual-licensed: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation. For the terms of this
 * license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
 * or contact us at https://server.io/contact/ when you want license
 * this software under a commercial license.
 */

package powermetersdm630

import (
	"encoding/json"
	"math"
	"net"
	"strconv"
	"strings"
	"time"

	"actshad.dev/modbus"
	"gitlab.com/perinet/generic/apiservice/dnssd"
	webhelper "gitlab.com/perinet/generic/lib/utils/webhelper"
)

var (
	modbus_client modbus.Client
	sample_cache  sampleCache
)

func modbus_is_connected() bool {
	return modbus_client != nil
}

// Read modbus register and convert result into float
func modbus_read_float_register(register_address uint16) (float32, error) {
	var result float32 = 0.0
	if modbus_client != nil {
		res, err := modbus_client.ReadInputRegisters(register_address, 2)
		if err != nil {
			modbus_client = nil
			return 0.0, err
		}
		var ieee754 uint32 = (uint32(res[0])<<24 + (uint32(res[1]) << 16) + (uint32(res[2]) << 8) + uint32(res[3]))
		result = math.Float32frombits(ieee754)
	}
	return result, nil
}

// Connect to modbus tcp bridge
func modbus_connect(host string, port uint, slave_id byte) error {
	connection := host + ":" + strconv.FormatUint(uint64(port), 10)
	Logger.Print("try connecting to " + connection)
	handler := modbus.NewTCPClientHandler(connection)
	handler.Timeout = 10 * time.Second
	handler.SlaveId = slave_id

	err := handler.Connect()
	if err != nil {
		Logger.Print(err)
		return err
	}
	Logger.Print("connected")

	defer handler.Close()
	modbus_client = modbus.NewClient(handler)
	return nil
}

// Return the value of a "key=value" string slice.
func get_txt_record(key string, txt_records []string) string {
	var ret string
	for _, txt_record := range txt_records {
		if strings.HasPrefix(txt_record, key) {
			res := strings.Split(txt_record, "=")
			ret = res[len(res)-1]
			break
		}
	}
	return ret
}

// Determine the network interface
// Deprecated: The to be used network interface needs to be taken from dns-sd
// as far as it is available there.
func get_net_interface() (string, error) {
	var ret string
	interfaces, err := net.Interfaces()
	if err != nil {
		return ret, err
	}
	for _, net_if := range interfaces {
		// obmit loopback device(s)
		if net_if.Flags&net.FlagLoopback != 0 {
			continue
		}
		ret = net_if.Name
		return ret, nil
	}
	return ret, nil
}

// Resolve modbus services by dns-sd and look for the configured modbus tcp
// bridge host.
func find_modbus_bridge_address() (string, int) {
	var network_interface string
	var address string
	port := 0

	// scan dedicated service(s) to find DNSSDServiceInfo belonging to hostname
	service_names := []string{"_mbap._tcp", "_mbap-s._tcp"}
	for _, service_name := range service_names {
		var vars = map[string]string{}
		vars["service_name"] = service_name
		data := webhelper.InternalVarsGet(dnssd.DNSSDServiceInfoDiscoverGet, vars)
		var service_info []dnssd.DNSSDServiceInfo
		err := json.Unmarshal(data, &service_info)
		if err != nil {
			continue
		}
		for _, service := range service_info {
			// check if resolved hostname equals configured hostname
			if service.Hostname == mbus_config.Hostname {
				// check if resolved application name equals configured application name
				host_app_name := get_txt_record("application_name", service.TxtRecord)
				if host_app_name == nodeInfo.Config.ApplicationName {
					address = service.Addresses[0].String()
					port = int(service.Port)
					network_interface, _ = get_net_interface()
				}
			}
		}
		if address != "" && network_interface != "" {
			address = "[" + address + "%" + network_interface + "]"
		} else {
			// If dnssd discovery doesn't work just use the configured host with default port
			res, _ := net.LookupHost(mbus_config.Hostname)
			if len(res) > 0 {
				address = "[" + res[0] + "]"
				port = 502
			}
		}
	}
	return address, port
}

// connect to modbus tcp bridge host. If connection is lost try to re-connect.
func keep_mbus_connection() {
	if len(mbus_config.Hostname) == 0 || len(nodeInfo.Config.ApplicationName) == 0 {
		return
	}

	var address string
	port := 0
	for { // ever
		time.Sleep(time.Millisecond * 500)
		if address == "" {
			address, port = find_modbus_bridge_address()
		} else if !modbus_is_connected() {
			modbus_connect(address, uint(port), byte(mbus_config.RtuAddress))
		}
		time.Sleep(time.Second)
	}
}

// Collect data of SDM-630 power meter
func collect_mbus_samples() {
	for { // ever
		if modbus_is_connected() && mbus_config.SampleRate > 0.0 {
			var sample PowermeterSDM630Sample
			for key, register := range SdmRegisters {
				res, err := modbus_read_float_register(register.Address)
				if err != nil {
					Logger.Printf("Could not read from %s (%s)", key, err)
					continue
				}
				switch key {
				case "L1Voltage":
					sample.L1Voltage.Value = res
					sample.L1Voltage.Unit = register.Unit
				case "L2Voltage":
					sample.L2Voltage.Value = res
					sample.L2Voltage.Unit = register.Unit
				case "L3Voltage":
					sample.L3Voltage.Value = res
					sample.L3Voltage.Unit = register.Unit
				case "L1Current":
					sample.L1Current.Value = res
					sample.L1Current.Unit = register.Unit
				case "L2Current":
					sample.L2Current.Value = res
					sample.L2Current.Unit = register.Unit
				case "L3Current":
					sample.L3Current.Value = res
					sample.L3Current.Unit = register.Unit
				case "L1PowerW":
					sample.L1Power.Value = res
					sample.L1Power.Unit = register.Unit
				case "L2PowerW":
					sample.L2Power.Value = res
					sample.L2Power.Unit = register.Unit
				case "L3PowerW":
					sample.L3Power.Value = res
					sample.L3Power.Unit = register.Unit
				case "L1PhaseAngle":
					sample.L1PhaseAngle.Value = res
					sample.L1PhaseAngle.Unit = register.Unit
				case "L2PhaseAngle":
					sample.L2PhaseAngle.Value = res
					sample.L2PhaseAngle.Unit = register.Unit
				case "L3PhaseAngle":
					sample.L3PhaseAngle.Value = res
					sample.L3PhaseAngle.Unit = register.Unit
				case "TotalImport":
					sample.TotalImport.Value = res
					sample.TotalImport.Unit = register.Unit
				case "TotalExport":
					sample.TotalExport.Value = res
					sample.TotalExport.Unit = register.Unit
				}
			}
			sample_cache.mutex.Lock()
			sample_cache.sample = sample
			sample_cache.mutex.Unlock()
		}
		time.Sleep(time.Millisecond * time.Duration(mbus_config.SampleRate*1000))
	}
}
