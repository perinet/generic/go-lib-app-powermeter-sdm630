# go-lib-app-powermeter-sdm630

The go-lib-app-powermeter-sdm630 is the abstraction layer of the
[Eastron SDM 630 modbus power meter](https://www.eastroneurope.com/products/view/sdm630modbus).
